package com.example.android_test_navigation

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels

class AFragment : Fragment(R.layout.a_fragment) {

    private val BFragment = BFragment()
    private var number = 0

    @SuppressLint("CommitTransaction")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        childFragmentManager.beginTransaction()
            .replace(R.id.flA, BFragment)
            .addToBackStack("BFragment")
            .commit()

        val btnA = view.findViewById<Button>(R.id.btnA)

        btnA.text = "btn A $number"
        btnA.setOnClickListener {
//            ++number
//            btnA.text = "btn A $number"
        }


    }
}