package com.example.android_test_navigation

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels

class BFragment : Fragment(R.layout.b_fragment) {

    private val shareViewModel: ShareViewModel by viewModels({ requireParentFragment() })
    private val CFragment = CFragment()
    private var number = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bt = view.findViewById<Button>(R.id.btB)

        if ((shareViewModel.x.value ?: 0) > 0) {
            parentFragmentManager.beginTransaction()
                .replace(R.id.flA, CFragment)
                .addToBackStack("CFragment")
                .commit()
        }

        bt.setOnClickListener {
            parentFragmentManager.beginTransaction()
                .replace(R.id.flA, CFragment)
                .addToBackStack("CFragment")
                .commit()
        }

        val btnB = view.findViewById<Button>(R.id.btnB)
        btnB.text = "B count $number"
        btnB.setOnClickListener {
//            ++number
//            btnB.text = "B count $number"

            shareViewModel.add()
        }


        shareViewModel.x.observe(viewLifecycleOwner) {
            btnB.text = "btn B $it"
        }
    }
}