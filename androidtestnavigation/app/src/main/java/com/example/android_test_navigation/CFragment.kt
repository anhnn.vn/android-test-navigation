package com.example.android_test_navigation

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController

class CFragment : Fragment(R.layout.c_fragment) {
    private val shareViewModel: ShareViewModel by viewModels({ requireParentFragment() })

    private var number = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bt = view.findViewById<Button>(R.id.btC)
        bt.setOnClickListener {
            findNavController().navigate(R.id.ProfileFragment)
        }


        val btnC = view.findViewById<Button>(R.id.btnC)

//        btnC.text = "C count $number"
//        btnC.setOnClickListener {
//            ++number
//            btnC.text = "C count $number"
//        }


        shareViewModel.x.observe(viewLifecycleOwner) {
            btnC.text = "C count $it"
        }
    }
}