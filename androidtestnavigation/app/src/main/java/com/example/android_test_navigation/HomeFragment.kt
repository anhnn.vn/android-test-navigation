package com.example.android_test_navigation

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class HomeFragment : Fragment(R.layout.home_fragment) {
    private lateinit var demoCollectionAdapter: CommonViewPagerAdapter
    private lateinit var viewPager: CustomViewPager
    private val AFragment = AFragment()
    private val AAFragment = AAFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        demoCollectionAdapter = CommonViewPagerAdapter(childFragmentManager)
        val list = mutableListOf(AFragment, AAFragment)
        demoCollectionAdapter.addListFragment(list)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewPager = view.findViewById(R.id.vp)

        view.findViewById<Button>(R.id.btnHome).setOnClickListener {
            (requireActivity() as MainActivity).navigateTo(R.id.ProfileFragment)
        }

        try {

            viewPager.adapter = demoCollectionAdapter
            viewPager.offscreenPageLimit = demoCollectionAdapter.count
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDestroyView() {
//        viewPager.adapter = null
//        demoCollectionAdapter = null
        super.onDestroyView()
    }
}

class DemoCollectionAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    var listFragment = mutableListOf<Fragment>()

    override fun getCount(): Int {
        return listFragment.count()
    }

    override fun getItem(position: Int) = listFragment[position]
}