package com.example.android_test_navigation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment

         navController = navHostFragment.navController

//       navigateTo(HomeFragment(), "HomeFragment")

    }

//    fun navigateTo(fragment: Fragment, tag: String) {
//        supportFragmentManager.beginTransaction()
//            .replace(R.id.flMain, fragment, tag)
//            .addToBackStack(tag)
//            .commit()
//    }

    fun navigateTo(id: Int) {
        navController.navigate(id)
    }

    fun back() {
        navController.popBackStack()
    }
}