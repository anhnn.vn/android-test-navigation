package com.example.android_test_navigation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ShareViewModel : ViewModel() {

    var x = MutableLiveData(0)

    fun add() {
        x.value = x.value?.plus(1)
    }
}